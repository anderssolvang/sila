class CreateSilas < ActiveRecord::Migration[6.0]
  def change
    create_table :silas do |t|
      t.string :sila_id, null: false
      t.string :ampel_id, null: true

      t.timestamps
    end
    add_index :silas, [:sila_id, :ampel_id], unique: true
  end
end
