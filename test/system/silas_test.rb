require "application_system_test_case"

class SilasTest < ApplicationSystemTestCase
  setup do
    @sila = silas(:one)
  end

  test "visiting the index" do
    visit silas_url
    assert_selector "h1", text: "Silas"
  end

  test "creating a Sila" do
    visit silas_url
    click_on "New Sila"

    fill_in "Ampel-id", with: @sila.ampel-id
    fill_in "Sila-id", with: @sila.sila-id
    click_on "Create Sila"

    assert_text "Sila was successfully created"
    click_on "Back"
  end

  test "updating a Sila" do
    visit silas_url
    click_on "Edit", match: :first

    fill_in "Ampel-id", with: @sila.ampel-id
    fill_in "Sila-id", with: @sila.sila-id
    click_on "Update Sila"

    assert_text "Sila was successfully updated"
    click_on "Back"
  end

  test "destroying a Sila" do
    visit silas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sila was successfully destroyed"
  end
end
