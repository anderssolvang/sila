require 'test_helper'

class SilasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sila = silas(:one)
  end

  test "should get index" do
    get silas_url
    assert_response :success
  end

  test "should get new" do
    get new_sila_url
    assert_response :success
  end

  test "should create sila" do
    assert_difference('Sila.count') do
      post silas_url, params: { sila: { ampel-id: @sila.ampel-id, sila-id: @sila.sila-id } }
    end

    assert_redirected_to sila_url(Sila.last)
  end

  test "should show sila" do
    get sila_url(@sila)
    assert_response :success
  end

  test "should get edit" do
    get edit_sila_url(@sila)
    assert_response :success
  end

  test "should update sila" do
    patch sila_url(@sila), params: { sila: { ampel-id: @sila.ampel-id, sila-id: @sila.sila-id } }
    assert_redirected_to sila_url(@sila)
  end

  test "should destroy sila" do
    assert_difference('Sila.count', -1) do
      delete sila_url(@sila)
    end

    assert_redirected_to silas_url
  end
end
