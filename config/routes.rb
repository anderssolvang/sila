Rails.application.routes.draw do


  # User authentication routes
  devise_for :users
  devise_scope :user do
    get 'sign-in', :to => 'devise/sessions#new'
    get 'account-update', to: 'devise/registrations#edit'
    get 'create-account', to: 'devise/registrations#new'
  end


  # Routes for the silas lights
  resources :silas, only: [:index, :show, :create, :destroy]


  # Root to if logged in
  authenticated :user do
    root to: 'silas#index', as: :authenticated_root
  end

  root to: 'silas#index'

end
