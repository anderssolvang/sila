module ApplicationHelper

  def helper_flash_type(flash_type)
    case flash_type
    when "alert"
      return "alert"
    # Else is the standard Notice flash
    else
      return "notice"
    end
  end

end
