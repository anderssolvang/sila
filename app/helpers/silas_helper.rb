module SilasHelper

  # returns sila or ampel string and number
  def helper_sila_or_ampel_id(light)
    return "#{intern_helper_ampel_or_sila(light)} #{light.match(/\d{1,3}/)}"
  end

  # Extracts ampel
  def intern_helper_ampel_or_sila(light)
    return light.match(/ampel/i).to_s.capitalize if light.match?(/ampel/i)
    return light.match(/euroskilt/i).to_s.capitalize if light.match?(/euroskilt/i)
  end

  # Gets Sila lights from portal and organises them into an nested array
  def helper_get_lights

    # Log into the portal
    mechanize = Mechanize.new
    page_full = mechanize.get('https://www.sila-signalbau.de/Login_Ampelueberwachung.htm?ActiveID=1226')
    form = page_full.form('form1')
    form['ctl01$tbUser'] = 'Drammen08'
    form['ctl01$tbPassword'] = 'Avdeling08'
    page = form.click_button

    # Search the page for all lights
    temp_list = page.search('tr td').map{ |x | x.text }
    lights_count = (temp_list.count / 11) - 1

    # Create an nested array for each light
    lights = Array.new

    # Add each light into appropriate array container
    temp_count = 1
    lights_count.times do
      lights << temp_list[(temp_count * 11)..((temp_count*11)+1)]
      temp_count += 1
    end

    return lights
  end

  # Create lights that are not there
  def helper_create_lights_if_not_there(light)
    sila_id = helper_sila_or_ampel_id(light[0][0])
    ampel_id = helper_sila_or_ampel_id(light[0][1])

    # Check if light exists, else create it
    Sila.where(sila_id: sila_id, ampel_id: ampel_id).first_or_create
  end
end
