json.extract! sila, :id, :sila-id, :ampel-id, :created_at, :updated_at
json.url sila_url(sila, format: :json)
