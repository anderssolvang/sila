class SilasController < ApplicationController
  include SilasHelper

  require 'mechanize'
  before_action :set_sila, only: [:show, :edit, :update, :destroy]

  # GET /silas
  # GET /silas.json
  def index
    # Process lights from current customer (saves new ones, and returns a count)
    @lights_count = find_lights

    # All silas lights from the DB is retrived from application controller - sila_lights



  end

  # GET /silas/1
  # GET /silas/1.json
  def show
  end

  # GET /silas/new
  def new
    @sila = Sila.new
  end

  # GET /silas/1/edit
  def edit
  end

  # POST /silas
  # POST /silas.json
  def create
    @sila = Sila.new(sila_params)

    respond_to do |format|
      if @sila.save
        format.html { redirect_to @sila, notice: 'Sila was successfully created.' }
        format.json { render :show, status: :created, location: @sila }
      else
        format.html { render :new }
        format.json { render json: @sila.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /silas/1
  # PATCH/PUT /silas/1.json
  def update
    respond_to do |format|
      if @sila.update(sila_params)
        format.html { redirect_to @sila, notice: 'Sila was successfully updated.' }
        format.json { render :show, status: :ok, location: @sila }
      else
        format.html { render :edit }
        format.json { render json: @sila.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /silas/1
  # DELETE /silas/1.json
  def destroy
    @sila.destroy
    respond_to do |format|
      format.html { redirect_to silas_url, notice: 'Sila was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def temp_lights

    # Log into the portal
    mechanize = Mechanize.new
    page_full = mechanize.get('https://www.sila-signalbau.de/Login_Ampelueberwachung.htm?ActiveID=1226')
    form = page_full.form('form1')
    form['ctl01$tbUser'] = 'Drammen08'
    form['ctl01$tbPassword'] = 'Avdeling08'
    page = form.click_button

    # Search the page for all lights
    temp_list = page.search('tr td').map{ |x | x.text }
    lights_count = (temp_list.count / 11) - 1

    # Create an nested array for each light
    lights = Array.new

    # Add each light into appropriate array container
    temp_count = 1
    lights_count.times do
      lights << temp_list[(temp_count * 11)..((temp_count*11)+10)]
      temp_count += 1
    end

    return lights
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sila
      @sila = Sila.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sila_params
      params.require(:sila).permit(:sila_id, :ampel_id)
    end


    # find and maybe create lights from portal page
    def find_lights
      # Scrapes Sila-sinalbau.de for sila lights from the customer
      lights_from_page = helper_get_lights

      # Create lights that are not present in DB
      lights_from_page.each do |light|
        helper_create_lights_if_not_there(light)
      end

      # Return count of lights
      return lights_from_page.count
    end
end
