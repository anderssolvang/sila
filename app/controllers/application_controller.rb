class ApplicationController < ActionController::Base

  before_action :all_sila_lights

  def all_sila_lights
    @silas = Sila.all
  end
end
